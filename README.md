# brendan.fyi

My own lil URL shortener

## Inspired by

This hosted on Netlify and was inspired by https://cass.run/shortener

## Make your own

Clicking this button will clone the repo and deploy it to Netlify, be sure to rename your repo afterwards!

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/cassidoo/cass.run&utm_source=github&utm_medium=shortener-cs&utm_campaign=devex)
